import Foundation

class GalleryManager {
    static let shared = GalleryManager()
    private init () {}

    private let keyGallery = "key"
    
    func getGallery() -> [GalleryModel] {
        if var settingsGallery = UserDefaults.standard.value([GalleryModel].self, forKey: keyGallery) {
            return settingsGallery
        }
        return []
    }
    
    func setGallery(_ settingsGallery: [GalleryModel]) {
        UserDefaults.standard.set(encodable: settingsGallery, forKey: keyGallery)
    }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
