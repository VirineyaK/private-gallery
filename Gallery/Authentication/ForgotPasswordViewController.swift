import UIKit
import FirebaseAuth

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createButtonDesign()
    }
    
    @IBAction func forgotPasswordButtonPressed(_ sender: UIButton) {
        let email = emailField.text!
        if(!email.isEmpty) {
            Auth.auth().sendPasswordReset(withEmail: email) { (error) in
                if error == nil {
                    self.showAlert(title: "Сообщение", message: "Ссылка на восстановление пароля отправлена на указанный электронный адрес.", titleBtn: "ОК")
                } else {
                    self.showAlert(title: "Ошибка", message: "Повторите попытку снова.", titleBtn: "ОК")
                }
            }
        }
    }
    
    @IBAction func goToAuthButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    private func showAlert(title: String, message: String, titleBtn: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: titleBtn, style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func createButtonDesign() {
        
        forgotPasswordButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        forgotPasswordButton.addGradient()
    }
}
