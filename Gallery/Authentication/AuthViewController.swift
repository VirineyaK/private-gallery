import UIKit
import Firebase
import KeychainSwift

class AuthViewController: UIViewController {
    
    private let keychain = KeychainSwift()
    
    private var signup: Bool = true {
        willSet {
            if newValue {
                titleLabel.text = "Регистрация"
                nameField.isHidden = false
                enterButton.setTitle("Вход", for: .normal)
                questionLabel.text = "У Вас уже есть аккаунт?"
                registrationAndLoginButton.setTitle("Зарегистрировать", for: .normal)
            } else {
                titleLabel.text = "Вход"
                nameField.isHidden = true
                enterButton.setTitle("Регистрация", for: .normal)
                questionLabel.text = "Хотите зарегистрировть аккаунт?"
                registrationAndLoginButton.setTitle("Войти", for: .normal)
            }
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var registrationAndLoginButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameField.delegate = self
        emailField.delegate = self
        passwordField.delegate = self
        
        self.getEmailAndPassword()
        self.createButtonDesign()
    }
    
    @IBAction func switchLoginButtonPressed(_ sender: UIButton) {
        signup = !signup
    }
    
    @IBAction func registrationAndLoginButtonPressed(_ sender: UIButton) {
        self.openRegistrationOrLogin()
        keychain.set(emailField.text ?? "", forKey: "email")
        keychain.set(passwordField.text ?? "", forKey: "password")
    }
    
    @IBAction func goToForgotPasswordButtonPressed(_ sender: UIButton) {
        guard let forgotController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController else {
            return
        }
        self.navigationController?.pushViewController(forgotController, animated: true)
    }
    
    private func openRegistrationOrLogin() {
        let name = nameField.text!
        let email = emailField.text!
        let password = passwordField.text!
        
        if signup {
            if !name.isEmpty && !email.isEmpty && !password.isEmpty {
                Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                    if error == nil {
                        if let result = result {
                            print(result.user.uid)
                            let ref = Database.database().reference().child("users")
                            ref.child(result.user.uid).updateChildValues(["name" : name, "email" : email])
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            } else {
                showAlert()
            }
        } else {
            if !email.isEmpty && !password.isEmpty {
                Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                    if error == nil {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                
            } else {
                showAlert()
            }
        }
    }
    
    func getEmailAndPassword() {
        let email = keychain.get("email")
        emailField.text = email
        let password = keychain.get("password")
        passwordField.text = password
    }
    
    private func showAlert() {
        let alert = UIAlertController(title: "Ошибка!", message: "Заполните все поля", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func createButtonDesign() {
        registrationAndLoginButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        registrationAndLoginButton.addGradient()
    }
}

extension AuthViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.openRegistrationOrLogin()
        return true
    }
}
