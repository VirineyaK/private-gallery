import UIKit

protocol ViewControllerDelegate: AnyObject {
    func updateCollectionView(_ arrayGallery: [GalleryModel]?)
}

class ViewPhotoController: UIViewController {
    
    weak var delegate: ViewControllerDelegate?
    var index = 0
    private var additionalImageView = UIImageView()
    private var galleryUserSettings: [GalleryModel]?
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var likeImageButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.galleryUserSettings = GalleryManager.shared.getGallery()
        mainImageView.image = FilesManager.shared.loadSavedImageFromDirectory(fileName: galleryUserSettings?[index].uuid ?? "")
        likeImageButton.isSelected = galleryUserSettings?[index].likeUser ?? true
        commentTextField.text = galleryUserSettings?[index].commentUser ?? " "
        
        self.hideKeyboard()
        self.registerForKeyboardNotifications()
        
        
        self.createImagesSwipe()
    }
    
    override func viewDidLayoutSubviews() {
        galleryUserSettings?[index].commentUser = commentTextField.text
        if let galleryUserSettings = self.galleryUserSettings {
            GalleryManager.shared.setGallery(galleryUserSettings)
        }
    }
    
    // MARK: - Saving favotite photo
    
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        
        if self.galleryUserSettings?[index].likeUser == true {
            self.galleryUserSettings?[index].likeUser = false
        } else {
            self.galleryUserSettings?[index].likeUser = true
        }
        
        if let galleryUserSettings = self.galleryUserSettings {
            GalleryManager.shared.setGallery(galleryUserSettings)
        }
        
        if likeImageButton.isSelected == true {
            likeImageButton.isSelected = false
        } else {
            likeImageButton.isSelected = true
        }
    }
    
    // MARK: - Photo view settings
    
    @IBAction func showPreviousImageSwipe(_ sender: UISwipeGestureRecognizer) {
        self.showPreviousImage()
        
        additionalImageView.frame = CGRect(x: self.view.frame.size.width + self.mainImageView.frame.size.width, y: mainImageView.frame.origin.y, width: view.frame.size.width, height: mainImageView.frame.size.height)
        
        likeImageButton.isSelected = galleryUserSettings?[index].likeUser ?? true
        commentTextField.text = galleryUserSettings?[index].commentUser ?? " "
        mainImageView.image = FilesManager.shared.loadSavedImageFromDirectory(fileName: galleryUserSettings?[index].uuid ?? "")
        self.galleryUserSettings = GalleryManager.shared.getGallery()
        
        additionalImageView.contentMode = .scaleAspectFit
        self.scrollView.addSubview(additionalImageView)
        
        UIImageView.animate(withDuration: 0.1, animations: {
            self.mainImageView.frame.origin.x = self.additionalImageView.frame.origin.x
        }) { (_) in
            self.additionalImageView.image = self.mainImageView.image
        }
    }
    
    
    
    @IBAction func showNextImageSwipe(_ sender: UISwipeGestureRecognizer) {
        self.showNextImage()
        
        additionalImageView.frame = CGRect(x: 0 - self.view.frame.size.width, y: mainImageView.frame.origin.y, width: view.frame.size.width, height: mainImageView.frame.size.height)
        mainImageView.image = FilesManager.shared.loadSavedImageFromDirectory(fileName: galleryUserSettings?[index].uuid ?? "")
        likeImageButton.isSelected = galleryUserSettings?[index].likeUser ?? true
        commentTextField.text = galleryUserSettings?[index].commentUser ?? " "
        
        self.galleryUserSettings = GalleryManager.shared.getGallery()
        
        additionalImageView.contentMode = .scaleAspectFit
        self.scrollView.addSubview(additionalImageView)
        
        UIImageView.animate(withDuration: 0.1, animations: {
            self.mainImageView.frame.origin.x = self.additionalImageView.frame.origin.x
        }) { (_) in
            self.additionalImageView.image = self.mainImageView.image
        }
    }
    
    
    @IBAction func closeViewPhotoController(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.delegate?.updateCollectionView(galleryUserSettings)
        GalleryManager.shared.setGallery(galleryUserSettings ?? [])
        
    }
    
    @IBAction func deleteImageButtonPressed(_ sender: Any) {
        self.showAlertConfirmationOfRemoval()
    }
    
    @IBAction func tapDetectedHidingKeyboard(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // MARK: - Swipe Photo
    
    private func createImagesSwipe() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(showPreviousImageSwipe(_ :)))
        leftSwipe.direction = .left
        self.view.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(showNextImageSwipe(_ :)))
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    private func showNextImage() {
        self.index -= 1
        if self.index == -1 {
            self.index = (galleryUserSettings?.count ?? 0) - 1
        }
    }
    
    private func showPreviousImage() {
        self.index += 1
        if self.index == galleryUserSettings?.count {
            self.index = 0
        }
    }
    
    // MARK: - Saving user comment
    
    private func saveUserComment() {
        likeImageButton.isSelected = galleryUserSettings?[index].likeUser ?? true
        commentTextField.text = galleryUserSettings?[index].commentUser ?? " "
        if let settingsUserGallery = self.galleryUserSettings {
            GalleryManager.shared.setGallery(settingsUserGallery)
        }
    }
    
    // MARK: - Remove photos from gallery
    
    private func showAlertConfirmationOfRemoval(messageAlertPassword: String = "Удалить выбранный элемент?") {
        let alertConfirmation = UIAlertController(title: "Внимание!", message: messageAlertPassword, preferredStyle: .alert)
        
        let alertButtonOk = UIAlertAction(title: "Да", style: .default) { (_) in
            self.galleryUserSettings?.remove(at: self.index)
            
            
            if self.index == -1 {
                self.index = (self.galleryUserSettings?.count ?? 0) - 1
            } else if self.index == self.galleryUserSettings?.count {
                self.index = 0
            }
            
            self.mainImageView.image = FilesManager.shared.loadSavedImageFromDirectory(fileName: self.galleryUserSettings?[self.index].uuid ?? "")
            self.likeImageButton.isSelected = self.galleryUserSettings?[self.index].likeUser ?? true
            self.commentTextField.text = self.galleryUserSettings?[self.index].commentUser ?? " "
        }
        
        let alertButtonCancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alertConfirmation.addAction(alertButtonOk)
        alertConfirmation.addAction(alertButtonCancel)
        self.present(alertConfirmation, animated: true, completion: nil)
    }
    
    // MARK: - Keyboard Notification
    
    private func hideKeyboard() {
        let recognizerTapHidingKeyboard = UITapGestureRecognizer(target: self, action: #selector(tapDetectedHidingKeyboard(_:)))
        self.view.addGestureRecognizer(recognizerTapHidingKeyboard)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        
        let userInfo = notification.userInfo!
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            bottomConstraint.constant = 0
        } else {
            bottomConstraint.constant = keyboardScreenEndFrame.height
            scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentSize.height)
        }
        
        view.needsUpdateConstraints()
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ViewPhotoController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        commentTextField.resignFirstResponder()
        return true
    }
}

extension Notification.Name {
    static let keyboardWillShowNotification = Notification.Name("keyboardWillShowNotification")
    static let keyboardWillHideNotification = Notification.Name("keyboardWillHideNotification")
}
