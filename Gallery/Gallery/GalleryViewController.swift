import UIKit

class GalleryViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var galleryArray: [GalleryModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.reloadData()
        self.galleryArray = GalleryManager.shared.getGallery()
    }
    
    @IBAction func goToStartingVCButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension GalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return galleryArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as? GalleryCollectionViewCell else {
            return GalleryCollectionViewCell()
        }
        guard let uuid = galleryArray?[indexPath.item].uuid else {
            return cell
        }
        cell.setImage(with: uuid)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth = self.view.frame.width
        return CGSize(width: screenWidth/3 - 10, height: screenWidth/3 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let nextController = self.storyboard?.instantiateViewController(withIdentifier: "ViewPhotoController") as? ViewPhotoController else {
            return
        }
        nextController.delegate = self
        nextController.index = indexPath.row
        self.navigationController?.pushViewController(nextController, animated: true)
    }
}

extension GalleryViewController: ViewControllerDelegate{
    func updateCollectionView(_ arrayGallery: [GalleryModel]?) {
        self.galleryArray = arrayGallery
        self.collectionView.reloadData()
    }
    
}

