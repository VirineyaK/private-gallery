import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    func showPhoto(with image: UIImage?) {
        imageView.image = image
    }
    
    override func prepareForReuse() {
        imageView.image = nil
    }
    
    func setImage (with image: String?) {
        self.imageView.image = FilesManager.shared.loadSavedImageFromDirectory(fileName: image ?? "")
    }
}

