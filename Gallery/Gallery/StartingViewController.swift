import UIKit
import Firebase

class StartingViewController: UIViewController{
    
    private let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var addPhotoToGalleryButton: UIButton!
    @IBOutlet weak var showGalleryButton: UIButton!
    @IBOutlet weak var sampleImageView: UIImageView!
    @IBOutlet weak var logOutButton: UIButton!
    
    private var galleryArray: [GalleryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker.delegate = self
        self.galleryArray = GalleryManager.shared.getGallery()
        
        if galleryArray == [GalleryModel]() {
            for element in BasicGallery.shared.basicArrayOfImages {
                if let image = element {
                    let currentImage = GalleryModel()
                    FilesManager.shared.saveImageToDirectory(image: image, userImage: currentImage)
                    galleryArray.append(currentImage)
                    GalleryManager.shared.setGallery(galleryArray)
                }
            }
        }
        self.createButtonDesign()
    }
    
    @IBAction func showGalleryButtonPressed(_ sender: UIButton) {
        guard let galleryController = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as? GalleryViewController else {
            return
        }
        self.navigationController?.pushViewController(galleryController, animated: true)
    }
    
    @IBAction func addPhotoToGalleryButton(_ sender: UIButton) {
        let alertAddPhoto = UIAlertController(title: "Хотите добавить фото?", message: "", preferredStyle: .actionSheet)
        let cameraButton = UIAlertAction(title: "Открыть камеру", style: .default) { (_) in
            self.pickCamera()
        }
        let photoButton = UIAlertAction(title: "Открыть галерею", style: .default) { (_) in
            self.pickPhoto()
        }
        let cancelButton = UIAlertAction(title: "Отмена", style: .destructive) { (_) in
        }
        
        alertAddPhoto.addAction(cameraButton)
        alertAddPhoto.addAction(photoButton)
        alertAddPhoto.addAction(cancelButton)
        self.present(alertAddPhoto, animated: true, completion: nil)
    }
    
    @IBAction func logOutFromAccountButtonPressed(_ sender: UIButton) {
        do {
            try Auth.auth().signOut()
        } catch {
            print(error)
        }
    }
    
    private func pickCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    private func pickPhoto() {
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    private func createButtonDesign() {
        addPhotoToGalleryButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        addPhotoToGalleryButton.addGradient()
        
        showGalleryButton.dropShadow(color: .gray, offSet: CGSize(width: 5, height: 5))
        showGalleryButton.addGradient()
        
        logOutButton.dropShadow(color: .gray, offSet: CGSize(width: 3, height: 3))
        logOutButton.addGradient()
    }
}

extension StartingViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.originalImage] as? UIImage {
            let currentImage = GalleryModel()
            FilesManager.shared.saveImageToDirectory(image: pickedImage, userImage: currentImage)
            galleryArray.append(currentImage)
            GalleryManager.shared.setGallery(galleryArray)
            sampleImageView.image = pickedImage
            picker.dismiss(animated: true, completion: nil)
        }
    }
}

extension UIButton {
    
    func dropShadow(color: UIColor, opacity: Float = 1.0, offSet: CGSize, radius: CGFloat = 0, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 0).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func addGradient() {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.white.cgColor, UIColor.lightGray.cgColor]
        gradient.startPoint = CGPoint(x: 1.0, y: 0.7)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.opacity = 1.0
        
        gradient.frame = self.bounds
        self.layer.addSublayer(gradient)
    }
}
