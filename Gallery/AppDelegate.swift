import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       FirebaseApp.configure()
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user == nil {
                self.showModalAuth()
            }
        }
        return true
    }
    
    func showModalAuth() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let authController = storyboard.instantiateViewController(withIdentifier: "AuthViewController") as! AuthViewController
        self.window?.rootViewController?.present(authController, animated: true, completion: nil)
        authController.modalPresentationStyle = .fullScreen
    }



}

