import Foundation

class GalleryModel: NSObject, Codable {
    
    var uuid: String?
    var imageUrls: URL?
    var likeUser: Bool?
    var commentUser: String?
    
    init(likeUser: Bool, commentUser: String, imageUrls: URL, uuid: String) {
        self.likeUser = likeUser
        self.commentUser = commentUser
        self.imageUrls = imageUrls
        self.uuid = uuid
    }
    
    public enum CodingKeys: String, CodingKey {
        case likeUser, commentUser, uuid, imageUrls
    }
    
    public override init() {
        uuid = UUID.init().uuidString
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.uuid, forKey: .uuid)
        try container.encode(self.imageUrls, forKey: .imageUrls)
        try container.encode(self.likeUser, forKey: .likeUser)
        try container.encode(self.commentUser, forKey: .commentUser)
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.uuid = try container.decodeIfPresent(String.self, forKey: .uuid) ?? String()
        self.imageUrls = try container.decodeIfPresent(URL.self, forKey: .imageUrls) ?? nil
        self.likeUser = try (container.decodeIfPresent(Bool.self, forKey: .likeUser) ?? true)
        self.commentUser = try (container.decodeIfPresent(String.self, forKey: .commentUser) ?? "commentUser")
    }
}
