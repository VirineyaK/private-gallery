import UIKit
import Foundation

class BasicGallery {

static let shared = BasicGallery ()
private init () {}

var basicArrayOfImages: [UIImage?] = [UIImage(named: "catOne"), UIImage(named: "catTwo"), UIImage(named: "catThree"), UIImage(named: "catFour"), UIImage(named: "catFive"), UIImage(named: "dogOne"), UIImage(named: "dogTwo")]
}
